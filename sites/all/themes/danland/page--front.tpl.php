<style type="text/css">
#user-login-form{
	margin-left:-163px;
	margin-top:105px;
}
.block-title{display:none;}
#layout-type-1 #sidebar-right{
	border-left:none;
}
</style>

<?php
// $Id: page.tpl.php,v 1.17.2.4 2010/11/19 14:42:44 danprobo Exp $
?>
<link rel="stylesheet" type="text/css" href="dropdown.css" />
<link rel="shortcut icon" href="favicon.ico">
<script src="scripts/drophover.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=IM+Fell+DW+Pica+SC' rel='stylesheet' type='text/css'>
<!--[if lte IE 9]>
<style type="text/css">
#nav li a.top_link {
	padding-left:0.62em;
}
</style>
<![endif]-->

<div <?php print danland_page_class($page['sidebar_first'], $page['sidebar_second']); ?>>
<div id="header">
<div id="header-wrapper">

<div id="logo">
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" width="514" height="117">
  <param name="movie" value="http://74.53.235.123/~microide/sites/all/themes/danland/logo.swf" />
  <param name="quality" value="high" />
  <param name="WMODE" value="Transparent" />
  <embed src="http://74.53.235.123/~microide/sites/all/themes/danland/logo.swf"  wmode="transparent" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="514" height="117"></embed></object>
</div>

	<?php if ($page['search_box']): ?>
		<div id="search-box">
			<?php print render ($page['search_box']); ?>
		</div><!-- /search-box -->
	<?php endif; ?>

	<?php if ($feed_icons): ?>
		<div class="feed-wrapper">
			<?php print $feed_icons; ?>
		</div>
	<?php endif; ?>

	<?php if (!$is_admin): ?>
		<div id="authorize">
      		      <ul><?php global $user; if ($user->uid != 0) { print '<li class="first">' .t('Logged in as '). '<a href="' .url('user/'.$user->uid). '">' .$user->name. '</a></li>'; print '<li><a href="' .url('user/logout'). '">' .t('Logout'). '</a></li>'; } else { print '<li class="first"><a href="' .url('user'). '">' .t('Login'). '</a></li>'; print ''; } ?></ul>
		</div>
	<?php endif; ?>
</div><!-- end header-wrapper -->
</div> <!-- /header -->

<div style="clear:both"></div>

<div id="menu-wrapper">
<ul id="nav">
	<li class="top"><a href="http://74.53.235.123/~microide/" class="top_link"><span>HOME</span></a></li>
	<li class="top"><a href="http://74.53.235.123/~microide/our-company" id="our-company" class="top_link"><span class="down">ABOUT US</span></a>
		<ul class="sub">
        	<li><a href="http://74.53.235.123/~microide/our-company">About Us</a></li>
			<li><a href="http://74.53.235.123/~microide/management">Management</a></li>
			<li><a href="http://74.53.235.123/~microide/board-of-directors">Board of Directors</a></li>
		</ul>
	</li>
	<li class="top"><a href="http://74.53.235.123/~microide/solutions" id="solutions" class="top_link"><span class="down">PRODUCTS</span></a>
		<ul class="sub">
			<li><a href="http://74.53.235.123/~microide/solutions" class="fly">MIT 1000</a>
            <ul>
					<li><a href="http://74.53.235.123/~microide/full-page-flyer">Full Page Flyer</a></li>
					<li><a href="http://74.53.235.123/~microide/mit-user-guide">MIT 1000 User Guide</a></li>
                    <li><a href="http://74.53.235.123/~microide/sample-making-guide">Sample Making Guide</a></li>
                    <li><a href="http://74.53.235.123/~microide/technical-white-paper">Technical White Paper</a></li>
            </ul>
            </li>
			<li><a href="#" class="fly">Validation</a>
            <ul>
					<li><a href="http://www.aoac.org/" target="_blank">AOAC Link</a></li>
            </ul>
            </li>
			<!--<li><a href="#" class="fly">Technology</a>
            <ul>
					<li><a href="http://74.53.235.123/~microide/websites/technical-white-paper.php">Technical White Paper</a></li>
            </ul>
            </li>-->
			<li><a href="#" class="fly">Identifiers</a>
            <ul>
					<li><a href="http://74.53.235.123/~microide/listeria-app">Listeria spp</a></li>
            </ul>
            </li>
            <li><a href="http://74.53.235.123/~microide/mit-parts-list">Consumables</a>
		</ul>
	</li>

	<li class="top"><a href="http://74.53.235.123/~microide/investors" id="investors" class="top_link"><span class="down">INVESTORS</span></a>
		<ul class="sub">
        	<li><a href="http://74.53.235.123/~microide/investors">Investors</a></li>
			<li><a href="http://74.53.235.123/~microide/stocks">Stock Information</a></li>
			<li><a href="http://74.53.235.123/~microide/sec-filings">SEC Filings</a></li>
		</ul>
	</li>

	<li class="top"><a href="http://74.53.235.123/~microide/blog" id="news" class="top_link"><span>NEWS</span></a></li>

	<!--<li class="top"><a href="#" id="media" class="top_link"><span>MEDIA</span></a></li>-->

    <!--<li class="top"><a href="node/2" id="store" class="top_link"><span>MIT STORE</span></a></li>-->

    <li class="top"><a href="http://74.53.235.123/~microide/contact" id="contact" class="top_link"><span>CONTACT US</span></a></li>

    <!--<li class="top"><a href="node/2" id="create-account" class="top_link"><span>CREATE AN ACCOUNT</span></a></li>-->
</ul>
</div>

<!--<div id="menu">
<div id="rounded-menu-left"></div>-->
 <?php /*?><?php if ($main_menu || $page['superfish_menu']): ?>
      <div id="<?php print $main_menu ? 'nav' : 'superfish' ; ?>">
        <?php 
					     if ($main_menu) {
		          print theme('links__system_main_menu', array('links' => $main_menu));  
				      }
				      elseif (!empty($page['superfish_menu'])) {
				        print render ($page['superfish_menu']);
				      }
        ?><?php */?>
      <!--</div>--> <!-- end primary -->
    <?php /*?><?php endif; ?><?php */?>

<!--<div id="rounded-menu-right"></div>
</div>--> <!-- end menu -->

<div style="clear:both"></div>

<div style="width:950px; height:15px; margin:0px auto; background:#d8d6d6;"></div>
<div id="content-wrapper">
<?php if($is_front): ?>
<div id="slideshow-wrapper">
<div class="slideshow-inner">
<div id="slideshow-preface">
 <?php if ($page['preface']): ?>
          <div id="preface">
            <?php print render ($page['preface']); ?>
          </div><!-- end preface -->
 <?php endif; ?>
</div>

<?php if ($page['highlighted']) : ?><div id="slideshow-bottom">
<div id="mission"><?php print render ($page['highlighted']); ?></div></div><?php endif; ?>
<div class="slideshow">
<img src="<?php print $base_path . $directory; ?>/images/slideshows/slide1.jpg" width="950" height="355" alt="slideshow 1"/>
<img src="<?php print $base_path . $directory; ?>/images/slideshows/slide2.jpg" width="950" height="355" alt="slideshow 2"/>
<!--<img src="--><?php /*?><?php print $base_path . $directory; ?><?php */?><!--/images/slideshows/snow.jpg" width="950" height="355" alt="slideshow 3"/>-->
</div>
</div>
</div>
<?php endif; ?>

<?php if($is_front): ?>
<div id="video-txt-panel">
<div id="video-container"><iframe width="480" height="320" src="http://www.youtube.com/embed/B-o9LEqrmSk?rel=0" frameborder="0" allowfullscreen></iframe></div>
<div id="video-txt">
<div class="video-txt" style="margin-top:65px;">
<p>Micro Imaging Technology (MMTC - News) is located in San Clemente, California and over the last several years has been working on the development of a breakthrough, laser-based, rapid microbial identification technology.</p>

<p>We are happy to announce that our concentrated effort has achieved success. Micro Imaging Technology has developed a product that significantly reduces the identification time of microbial contamination at a very economical cost.</p>

</div>
</div>
</div>
<?php endif; ?>
</div>

<div style="clear:both"></div>

 <?php if($page['preface_first'] || $page['preface_middle'] || $page['preface_last']) : ?>
    <div style="clear:both"></div>
    <div id="preface-wrapper" class="in<?php print (bool) $page['preface_first'] + (bool) $page['preface_middle'] + (bool) $page['preface_last']; ?>">
          <?php if($page['preface_first']) : ?>
          <div class="column A">
            <?php print render ($page['preface_first']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['preface_middle']) : ?>
          <div class="column B">
            <?php print render ($page['preface_middle']); ?>
          </div>
          <?php endif; ?>

          <?php if($page['preface_last']) : ?>
          <div class="column C">
            <?php print render ($page['preface_last']); ?>
          </div>

          <?php endif; ?>

      <div style="clear:both"></div>
    </div>
    <?php endif; ?>

<div style="clear:both"></div>

<?php if($is_front): ?>
<div id="column-wrapper">
<div id="column-left">
<div class="column-txt" style="margin-top:75px;">
<h2>Bacterial Identification in Under 5 Minutes... For Less than 15 cents.</h2>
<p>The Micro Identification Technologies (MIT) MIT 1000 is a rapid, laser based bacteria detection and identification technology. The MIT 1000 is a non-biological bacteria identification system that is extremely easy to use. It does not rely on chemical processing, fluorescent tags ot DNA analysis.</p>
<div class="read-more"><a href="mit-1000">Read More...</a></div>
</div>
</div>

<div id="column-center">
<div id="rss-container" style="margin-left:13px">
<embed src="http://7feeds.com/rssinformer.swf?rssinformer.swf" width="303" height="300" id="informer" name="informer" bgcolor="0xd1e0ff" quality="high" wmode="transparent" align="middle" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="http://7feeds.com/rssinformer.swf?version=1.4&title_format=center/_sans/16/0x000000/b/ni/nu&subtitle_text=Powered by&subtitle_format=center/verdana/11/0x000000/b/ni/nu&feed_title_format=left/arial/14/0x6599ff/b/ni/nu&feed_date_format=left/arial/12/0xC8BDBD/b/ni/nu&feed_copy_format=left/arial/12/0xC8BDBD/b/i/nu&feed_text_format=left/arial/12/0x666666/nb/ni/nu&body_bgcolor=0xd1e0ff&title_bgcolor=0xEFEFEF&subtitle_bgcolor=0xE3E3E3&footer_bgcolor=0xd1e0ff&feed_bgcolor=0xffffff&feed_highlight=0xe7f0ff&border_color=0xffffff&summary_length=300&scroll_speed=50&item_spacing=12&scroll_buttons=1&num_of_entries=5&pause_time=3000&show_publish_time=1&show_entry_summary=1&show_entry_copyright=0&show_footer=1&use_rounded_corners=0&open_new_window=1&data_url=http://7feeds.com/feed.php&feed_url=http%3A%2F%2Ffeeds%2Efinance%2Eyahoo%2Ecom%2Frss%2F2%2E0%2Fheadline%3Fs%3DMMTC%26region%3DUS%26lang%3Den%2DUS&strip_tags=0&footer_powered_format=right/_sans/11/0x666666/nb/ni/nu&footer_feedo_format=left/_sans/11/0x6599ff/nb/ni/nu&x_size=303&y_size=300&feed_back_color=0xf7faff&feeds_num_color=0x6599ff&buttons_onpress_color=0x6599ff&buttons_rollout_color=0x989898&show_body=0&widget_name=MIT%20News&used_theme=1&lang1=Loading...&lang2=Powered by"/><!--<div style="text-align:center; font-size:11px;"><a href="http://7feeds.com/">7feeds</a></div><div style="text-align:center; font-size:11px;"><a href="http://7feeds.com">http://7feeds.com</a></div>-->
</div>
</div>

<div id="column-right">
<div id="rss-container">
<embed src="http://7feeds.com/rssinformer.swf?rssinformer.swf" width="303" height="300" id="informer" name="informer" bgcolor="0xd1e0ff" quality="high" wmode="transparent" align="middle" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="http://7feeds.com/rssinformer.swf?version=1.4&title_format=center/_sans/16/0x000000/b/ni/nu&subtitle_text=Powered by&subtitle_format=center/verdana/11/0x000000/b/ni/nu&feed_title_format=left/arial/14/0x6599ff/b/ni/nu&feed_date_format=left/arial/12/0xC8BDBD/b/ni/nu&feed_copy_format=left/arial/12/0xC8BDBD/b/i/nu&feed_text_format=left/arial/12/0x666666/nb/ni/nu&body_bgcolor=0xd1e0ff&title_bgcolor=0xEFEFEF&subtitle_bgcolor=0xE3E3E3&footer_bgcolor=0xd1e0ff&feed_bgcolor=0xffffff&feed_highlight=0xe7f0ff&border_color=0xffffff&summary_length=300&scroll_speed=50&item_spacing=12&scroll_buttons=1&num_of_entries=5&pause_time=3000&show_publish_time=1&show_entry_summary=1&show_entry_copyright=0&show_footer=1&use_rounded_corners=0&open_new_window=1&data_url=http://7feeds.com/feed.php&feed_url=http%3A%2F%2Fwww%2Egoogle%2Ecom%2Falerts%2Ffeeds%2F03193487840192133434%2F16005335099021131383&strip_tags=0&footer_powered_format=right/_sans/11/0x666666/nb/ni/nu&footer_feedo_format=left/_sans/11/0x6599ff/nb/ni/nu&x_size=303&y_size=300&feed_back_color=0xf7faff&feeds_num_color=0x6599ff&buttons_onpress_color=0x6599ff&buttons_rollout_color=0x989898&show_body=0&widget_name=Food%20Safety%20News&used_theme=1&lang1=Loading...&lang2=Powered by"/><!--<div style="text-align:center; font-size:11px;"><a href="http://7feeds.com/">7feeds</a></div><div style="text-align:center; font-size:11px;"><a href="http://7feeds.com">http://7feeds.com</a></div>-->
</div>

 <?php /*?><?php if ($page['sidebar_second']): ?><?php */?>
      <!--<div id="sidebar-right" class="column sidebar">
      <div class="section">-->
        <?php /*?><?php print render($page['sidebar_second']); ?><?php */?>
      <!--</div>
      </div>--> <!-- end sidebar-second -->
    <?php /*?><?php endif; ?><?php */?>
</div>
</div> <!-- end wrapper -->

<?php endif; ?>

<?php if($page['bottom_first'] || $page['bottom_middle'] || $page['bottom_last']) : ?>
    <div style="clear:both"></div>
    <div id="bottom-teaser" class="in<?php print (bool) $page['bottom_first'] + (bool) $page['bottom_middle'] + (bool) $page['bottom_last']; ?>">
          <?php if($page['bottom_first']) : ?>
          <div class="column A">
            <?php print render ($page['bottom_first']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_middle']) : ?>
          <div class="column B">
            <?php print render ($page['bottom_middle']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_last']) : ?>
          <div class="column C">
            <?php print render ($page['bottom_last']); ?>
          </div>
          <?php endif; ?>
      <div style="clear:both"></div>
    </div> <!-- end bottom first etc. -->
    <?php endif; ?>

 <?php if($page['bottom_1'] || $page['bottom_2'] || $page['bottom_3'] || $page['bottom_4']) : ?>
    <div style="clear:both"></div><!-- Do not touch -->
    <div id="bottom-wrapper" class="in<?php print (bool) $page['bottom_1'] + (bool) $page['bottom_2'] + (bool) $page['bottom_3'] + (bool) $page['bottom_4']; ?>">
          <?php if($page['bottom_1']) : ?>
          <div class="column A">
            <?php print render ($page['bottom_1']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_2']) : ?>
          <div class="column B">
            <?php print render ($page['bottom_2']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_3']) : ?>
          <div class="column C">
            <?php print render ($page['bottom_3']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_4']) : ?>
          <div class="column D">
            <?php print render ($page['bottom_4']); ?>
          </div>
          <?php endif; ?>
      <div style="clear:both"></div>
    </div><!-- end bottom -->
    <?php endif; ?>

<div style="clear:both"></div>
<div id="footer-wrapper">

<div id="footer">

<div class="footer-nav"><a href="http://74.53.235.123/~microide/">home</a><a href="http://74.53.235.123/~microide/our-company">our company</a><a href="http://74.53.235.123/~microide/solutions">solutions</a><a href="http://74.53.235.123/~microide/investors">investors</a><a href="http://74.53.235.123/~microide/blog">news</a><a href="http://74.53.235.123/~microide/media">media</a><a href="http://74.53.235.123/~microide/products">mit store</a><a href="http://74.53.235.123/~microide/contact">contact us</a><a href="http://74.53.235.123/~microide/user/register">create an account</a><a href="http://74.53.235.123/~microide/privacy-policy">privacy policy</a><a href="http://74.53.235.123/~microide/sitemap">sitemap</a></div>
</div>

</div> <!-- end footer wrapper -->

<div style="clear:both"></div>

<!--<div id="notice"><p>Theme by <a href="http://www.danetsoft.com">Danetsoft</a> and <a href="http://www.danpros.com">Danang Probo Sayekti</a> inspired by <a href="http://www.maksimer.no">Maksimer</a></p></div>-->
</div>