


<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>mozilla/pdf.js · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <link rel="logo" type="image/svg" href="https://github-media-downloads.s3.amazonaws.com/github-logo.svg" />
    <meta property="og:image" content="https://github.global.ssl.fastly.net/images/modules/logos_page/Octocat.png">
    <meta name="hostname" content="fe17.rs.github.com">
    <link rel="assets" href="https://github.global.ssl.fastly.net/">
    <link rel="xhr-socket" href="/_sockets" />
    
    


    <meta name="msapplication-TileImage" content="/windows-tile.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="selected-link" value="repo_source" data-pjax-transient />
    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="github" name="octolytics-app-id" />

    
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="/CFaXy93qTcrD92L1c96664KRwLVhA3GbWH98rB4GJo=" name="csrf-token" />

    <link href="https://github.global.ssl.fastly.net/assets/github-f913aa6cea9c2be873a594b06df4bdd56c61a47a.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://github.global.ssl.fastly.net/assets/github2-73cb9c180ee487edb3f37f7f9b1cbf7bd6ebe204.css" media="all" rel="stylesheet" type="text/css" />
    


      <script src="https://github.global.ssl.fastly.net/assets/frameworks-e8054ad804a1cf9e9849130fee5a4a5487b663ed.js" type="text/javascript"></script>
      <script src="https://github.global.ssl.fastly.net/assets/github-97664a532fc6064b297cd0484a0b7bfda19f46b4.js" type="text/javascript"></script>
      
      <meta http-equiv="x-pjax-version" content="6eeba794374d861b396b25d9719604b1">

        <meta property="og:title" content="pdf.js"/>
  <meta property="og:type" content="githubog:gitrepository"/>
  <meta property="og:url" content="https://github.com/mozilla/pdf.js"/>
  <meta property="og:image" content="https://github.global.ssl.fastly.net/images/gravatars/gravatar-user-420.png"/>
  <meta property="og:site_name" content="GitHub"/>
  <meta property="og:description" content="pdf.js - PDF Reader in JavaScript"/>

  <meta name="description" content="pdf.js - PDF Reader in JavaScript" />

  <meta content="131524" name="octolytics-dimension-user_id" /><meta content="mozilla" name="octolytics-dimension-user_login" /><meta content="1663468" name="octolytics-dimension-repository_id" /><meta content="mozilla/pdf.js" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="1663468" name="octolytics-dimension-repository_network_root_id" /><meta content="mozilla/pdf.js" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/mozilla/pdf.js/commits/master.atom" rel="alternate" title="Recent Commits to pdf.js:master" type="application/atom+xml" />

  </head>


  <body class="logged_out  windows vis-public env-production ">

    <div class="wrapper">
      
      
      


      
      <div class="header header-logged-out">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions">
        <a class="button primary" href="/signup">Sign up</a>
      <a class="button" href="/login?return_to=%2Fmozilla%2Fpdf.js">Sign in</a>
    </div>

    <div class="command-bar js-command-bar  in-repository">


      <ul class="top-nav">
          <li class="explore"><a href="/explore">Explore</a></li>
        <li class="features"><a href="/features">Features</a></li>
          <li class="enterprise"><a href="https://enterprise.github.com/">Enterprise</a></li>
          <li class="blog"><a href="/blog">Blog</a></li>
      </ul>
        <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">

<input type="text" data-hotkey="/ s" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" autocapitalize="off"
    
    
      data-repo="mozilla/pdf.js"
      data-branch="master"
      data-sha="7dfdc95d319f565d53ae2c014521bfb7819ec764"
  >

    <input type="hidden" name="nwo" value="mozilla/pdf.js" />

    <div class="select-menu js-menu-container js-select-menu search-context-select-menu">
      <span class="minibutton select-menu-button js-menu-target">
        <span class="js-select-button">This repository</span>
      </span>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container">
        <div class="select-menu-modal">

          <div class="select-menu-item js-navigation-item js-this-repository-navigation-item selected">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" class="js-search-this-repository" name="search_target" value="repository" checked="checked" />
            <div class="select-menu-item-text js-select-button-text">This repository</div>
          </div> <!-- /.select-menu-item -->

          <div class="select-menu-item js-navigation-item js-all-repositories-navigation-item">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" name="search_target" value="global" />
            <div class="select-menu-item-text js-select-button-text">All repositories</div>
          </div> <!-- /.select-menu-item -->

        </div>
      </div>
    </div>

  <span class="octicon help tooltipped downwards" title="Show command bar help">
    <span class="octicon octicon-question"></span>
  </span>


  <input type="hidden" name="ref" value="cmdform">

</form>
    </div>

  </div>
</div>


      


          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        

<ul class="pagehead-actions">


  <li>
  <a href="/login?return_to=%2Fmozilla%2Fpdf.js"
  class="minibutton with-count js-toggler-target star-button entice tooltipped upwards "
  title="You must be signed in to use this feature" rel="nofollow">
  <span class="octicon octicon-star"></span>Star
</a>
<a class="social-count js-social-count" href="/mozilla/pdf.js/stargazers">
  6,536
</a>

  </li>

    <li>
      <a href="/login?return_to=%2Fmozilla%2Fpdf.js"
        class="minibutton with-count js-toggler-target fork-button entice tooltipped upwards"
        title="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-git-branch"></span>Fork
      </a>
      <a href="/mozilla/pdf.js/network" class="social-count">
        1,066
      </a>
    </li>
</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="repo-label"><span>public</span></span>
          <span class="mega-octicon octicon-repo"></span>
          <span class="author">
            <a href="/mozilla" class="url fn" itemprop="url" rel="author"><span itemprop="title">mozilla</span></a></span
          ><span class="repohead-name-divider">/</span><strong
          ><a href="/mozilla/pdf.js" class="js-current-repository js-repo-home-link">pdf.js</a></strong>

          <span class="page-context-loader">
            <img alt="Octocat-spinner-32" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">

      <div class="repository-with-sidebar repo-container with-full-navigation">

        <div class="repository-sidebar">
            

<div class="repo-nav repo-nav-full js-repository-container-pjax js-octicon-loaders">
  <div class="repo-nav-contents">
    <ul class="repo-menu">
      <li class="tooltipped leftwards" title="Code">
        <a href="/mozilla/pdf.js" aria-label="Code" class="js-selected-navigation-item selected" data-gotokey="c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_tags repo_branches /mozilla/pdf.js">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

        <li class="tooltipped leftwards" title="Issues">
          <a href="/mozilla/pdf.js/issues" aria-label="Issues" class="js-selected-navigation-item js-disable-pjax" data-gotokey="i" data-selected-links="repo_issues /mozilla/pdf.js/issues">
            <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
            <span class='counter'>468</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>

      <li class="tooltipped leftwards" title="Pull Requests"><a href="/mozilla/pdf.js/pulls" aria-label="Pull Requests" class="js-selected-navigation-item js-disable-pjax" data-gotokey="p" data-selected-links="repo_pulls /mozilla/pdf.js/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class='counter'>28</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


        <li class="tooltipped leftwards" title="Wiki">
          <a href="/mozilla/pdf.js/wiki" aria-label="Wiki" class="js-selected-navigation-item " data-pjax="true" data-selected-links="repo_wiki /mozilla/pdf.js/wiki">
            <span class="octicon octicon-book"></span> <span class="full-word">Wiki</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>
    </ul>
    <div class="repo-menu-separator"></div>
    <ul class="repo-menu">

      <li class="tooltipped leftwards" title="Pulse">
        <a href="/mozilla/pdf.js/pulse" aria-label="Pulse" class="js-selected-navigation-item " data-pjax="true" data-selected-links="pulse /mozilla/pdf.js/pulse">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped leftwards" title="Graphs">
        <a href="/mozilla/pdf.js/graphs" aria-label="Graphs" class="js-selected-navigation-item " data-pjax="true" data-selected-links="repo_graphs repo_contributors /mozilla/pdf.js/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped leftwards" title="Network">
        <a href="/mozilla/pdf.js/network" aria-label="Network" class="js-selected-navigation-item js-disable-pjax" data-selected-links="repo_network /mozilla/pdf.js/network">
          <span class="octicon octicon-git-branch"></span> <span class="full-word">Network</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

    </ul>

  </div>
</div>

            <div class="only-with-full-nav">
              

  

<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>

  <input type="text" class="clone js-url-field"
         value="https://github.com/mozilla/pdf.js.git" readonly="readonly">

  <span class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/mozilla/pdf.js.git" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
</div>

  

<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>

  <input type="text" class="clone js-url-field"
         value="https://github.com/mozilla/pdf.js" readonly="readonly">

  <span class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/mozilla/pdf.js" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
</div>



<p class="clone-options">You can clone with
    <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>,
    <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>,
  and <a href="https://help.github.com/articles/which-remote-url-should-i-use">other methods.</a>
</p>


  <a href="http://windows.github.com" class="minibutton sidebar-button">
    <span class="octicon octicon-device-desktop"></span>
    Clone in Desktop
  </a>

                <a href="/mozilla/pdf.js/archive/master.zip"
                   class="minibutton sidebar-button"
                   title="Download this repository as a zip file"
                   rel="nofollow">
                  <span class="octicon octicon-cloud-download"></span>
                  Download ZIP
                </a>
            </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          
<div class="js-info-carrier" data-show-full-navigation="yes"></div>

<div class="repository-meta js-details-container ">
    <div class="repository-description js-details-show">
      <p>PDF Reader in JavaScript</p>
    </div>



</div>

<div class="capped-box overall-summary ">

  <div class="stats-switcher-viewport js-stats-switcher-viewport">

    <ul class="numbers-summary">
      <li class="commits">
        <a data-pjax href="/mozilla/pdf.js/commits/master">
          <span class="num">
            <span class="octicon octicon-history"></span>
            5,379
          </span>
          commits
        </a>
      </li>
      <li>
        <a data-pjax href="/mozilla/pdf.js/branches">
          <span class="num">
            <span class="octicon octicon-git-branch"></span>
            2
          </span>
          branches
        </a>
      </li>

      <li>
        <a data-pjax href="/mozilla/pdf.js/releases">
          <span class="num">
            <span class="octicon octicon-tag"></span>
            5
          </span>
          releases
        </a>
      </li>

      <li>
        <a href="/mozilla/pdf.js/contributors">
          <span class="num">
            <span class="octicon octicon-organization"></span>
            106
          </span>
          contributors
        </a>
      </li>
    </ul>

      <div class="repository-lang-stats">
        <ol class="repository-lang-stats-numbers">
          <li>
              <a href="/mozilla/pdf.js/search?l=javascript">
                <span class="color-block language-color" style="background-color:#f15501;"></span>
                <span class="lang">JavaScript</span>
                <span class="percent">98.1%</span>
              </a>
          </li>
          <li>
              <a href="/mozilla/pdf.js/search?l=python">
                <span class="color-block language-color" style="background-color:#3581ba;"></span>
                <span class="lang">Python</span>
                <span class="percent">1.9%</span>
              </a>
          </li>
        </ol>
      </div>
  </div>

</div>

  <a href="#"
     class="repository-lang-stats-graph js-toggle-lang-stats tooltipped downwards"
     title="Show language statistics"
     style="background-color:#3581ba">
  <span class="language-color" style="width:98.1%; background-color:#f15501;" itemprop="keywords">JavaScript</span><span class="language-color" style="width:1.9%; background-color:#3581ba;" itemprop="keywords">Python</span>
  </a>




<div class="file-navigation in-mid-page">
    <a href="/mozilla/pdf.js/compare" class="minibutton compact primary tooltipped downwards" title="Compare &amp; review" data-pjax>
      <span class="octicon octicon-git-compare"></span>
    </a>

  


<div class="select-menu js-menu-container js-select-menu" >
  <span class="minibutton select-menu-button js-menu-target" data-hotkey="w"
    data-master-branch="master"
    data-ref="master">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax>

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-remove-close js-menu-close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/mozilla/pdf.js/tree/gh-pages" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="gh-pages" data-skip-pjax="true" rel="nofollow" title="gh-pages">gh-pages</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/mozilla/pdf.js/tree/master" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="master" data-skip-pjax="true" rel="nofollow" title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/mozilla/pdf.js/tree/v0.5.5" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.5.5" data-skip-pjax="true" rel="nofollow" title="v0.5.5">v0.5.5</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/mozilla/pdf.js/tree/v0.4.11" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.4.11" data-skip-pjax="true" rel="nofollow" title="v0.4.11">v0.4.11</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/mozilla/pdf.js/tree/v0.3.459" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.3.459" data-skip-pjax="true" rel="nofollow" title="v0.3.459">v0.3.459</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/mozilla/pdf.js/tree/v0.1.0" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.1.0" data-skip-pjax="true" rel="nofollow" title="v0.1.0">v0.1.0</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/mozilla/pdf.js/tree/milestone-0.2" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="milestone-0.2" data-skip-pjax="true" rel="nofollow" title="milestone-0.2">milestone-0.2</a>
            </div> <!-- /.select-menu-item -->
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->


  <div class="breadcrumb"><span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/mozilla/pdf.js" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">pdf.js</span></a></span></span><span class="separator"> / </span><form action="/login?return_to=%2Fmozilla%2Fpdf.js" class="js-new-blob-form tooltipped rightwards new-file-link" method="post" title="Sign in to make or propose changes"><span class="js-new-blob-submit octicon octicon-file-add"></span></form></div>
</div>



<a href="/mozilla/pdf.js/find/master"
  data-hotkey="t" style="display:none" data-pjax>Show File Finder</a>
<a href="/mozilla/pdf.js/ctags-search/master"
  data-hotkey="T" style="display:none" data-pjax>Show Definition Finder</a>
<div class="bubble files-bubble">
  <table class="files" data-pjax>
    <thead>

        
  <div class="commit commit-tease js-details-container" >
    <p class="commit-title ">
        <a href="/mozilla/pdf.js/commit/1bbf0ac50ba460c39f7a5a329a7a28134f3bca5b" class="message" data-pjax="true" title="Update README.md">Update README.md</a>
        
    </p>
    <div class="commit-meta">
      <span class="js-zeroclipboard zeroclipboard-link" data-clipboard-text="1bbf0ac50ba460c39f7a5a329a7a28134f3bca5b" data-copied-hint="copied!" title="Copy SHA"><span class="octicon octicon-clippy"></span></span>
      <a href="/mozilla/pdf.js/commit/1bbf0ac50ba460c39f7a5a329a7a28134f3bca5b" class="sha-block" data-pjax>latest commit <span class="sha">1bbf0ac50b</span></a>

      <div class="authorship">
        <img class="gravatar" height="20" src="https://secure.gravatar.com/avatar/d33a62b635bb5d4f87235b86d853cb8a?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" />
        <span class="author-name"><a href="/brendandahl" data-skip-pjax="true" rel="contributor">brendandahl</a></span>
        authored <time class="js-relative-date updated" datetime="2013-07-29T13:16:14-07:00" title="2013-07-29 13:16:14">July 29, 2013</time>

      </div>
    </div>
  </div>

    </thead>

    
<tbody class=""
  data-url="/mozilla/pdf.js/file-list/master">
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-directory"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/tree/master/examples" class="js-directory-link" id="bfebe34154a0dfd9fc7b447fc9ed74e9-f27c5d39b94216830592e1b925636402376cf830" title="examples">examples</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/2ac858fc68b76bf5ac774dbc4c2defab6027a931" class="message" data-pjax="true" title="Fixing #3318">Fixing</a> <a href="https://github.com/mozilla/pdf.js/issues/3318" class="issue-link" title="Silly typo in hello.js">#3318</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-06-02T14:51:08-07:00" title="2013-06-02 14:51:08">June 02, 2013</time></span></td>
    </tr>
    <tr class="">
      <td class="icon">
        <span class="octicon octicon-file-directory"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/tree/master/extensions" class="js-directory-link" id="2ac737d240fc746cef37129b7569f08e-25b6e2a8e58ea820eda494dbf18a434d2aa6a54e" title="extensions">extensions</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/592a498dd7a6b0f76e767a1ced9a2e432cb5dd0a" class="message" data-pjax="true" title="Merge pull request #3515 from Rob--W/crx-webnav-strip-hash

[CRX] Strip location fragment from URL in the UrlFilter of the webNavigation API">Merge pull request</a> <a href="https://github.com/mozilla/pdf.js/issues/3515" class="issue-link" title="[CRX] Strip location fragment from URL in the UrlFilter of the webNavigation API">#3515</a> <a href="/mozilla/pdf.js/commit/592a498dd7a6b0f76e767a1ced9a2e432cb5dd0a" class="message" data-pjax="true" title="Merge pull request #3515 from Rob--W/crx-webnav-strip-hash

[CRX] Strip location fragment from URL in the UrlFilter of the webNavigation API">from Rob--W/crx-webnav-strip-hash</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-29T10:07:59-07:00" title="2013-07-29 10:07:59">July 29, 2013</time></span></td>
    </tr>
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-directory"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/tree/master/external" class="js-directory-link" id="6a21b6995a068148bbb65c8f949b3fb2-4591c2db5f68bdac5d98d0a821e9253216da3bdf" title="external">external</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/6b9c27259036b6aeb7bb5862e1d88b0ef7e5995d" class="message" data-pjax="true" title="Merge pull request #3457 from yurydelendik/remove-prefixes

Removes foreign for Firefox CSS prefixes">Merge pull request</a> <a href="https://github.com/mozilla/pdf.js/issues/3457" class="issue-link" title="Removes foreign for Firefox CSS prefixes">#3457</a> <a href="/mozilla/pdf.js/commit/6b9c27259036b6aeb7bb5862e1d88b0ef7e5995d" class="message" data-pjax="true" title="Merge pull request #3457 from yurydelendik/remove-prefixes

Removes foreign for Firefox CSS prefixes">from yurydelendik/remove-prefixes</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-16T16:08:05-07:00" title="2013-07-16 16:08:05">July 16, 2013</time></span></td>
    </tr>
    <tr class="">
      <td class="icon">
        <span class="octicon octicon-file-directory"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/tree/master/l10n" class="js-directory-link" id="2e27ceb02c60deb0520438c9831249a4-4729037b66c7ba07785aaa4691e9ad2ecfcba047" title="l10n">l10n</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/cd0fb0d484022844d974aba8349e9823ac299de9" class="message" data-pjax="true" title="Merge pull request #3247 from antapos/greeklangsupport

Added localization for greek language">Merge pull request</a> <a href="https://github.com/mozilla/pdf.js/issues/3247" class="issue-link" title="Added localization for greek language">#3247</a> <a href="/mozilla/pdf.js/commit/cd0fb0d484022844d974aba8349e9823ac299de9" class="message" data-pjax="true" title="Merge pull request #3247 from antapos/greeklangsupport

Added localization for greek language">from antapos/greeklangsupport</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-05-30T13:45:36-07:00" title="2013-05-30 13:45:36">May 30, 2013</time></span></td>
    </tr>
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-directory"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/tree/master/src" class="js-directory-link" id="25d902c24283ab8cfbac54dfa101ad31-6a2346b3ea7d748ad051573a5df242ef28e2b5ad" title="src">src</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/95bb727931acd2776bb51544b3864d963f4f1f15" class="message" data-pjax="true" title="Merge pull request #3494 from SSk123/master

Fixing the offset of vertical CJK text">Merge pull request</a> <a href="https://github.com/mozilla/pdf.js/issues/3494" class="issue-link" title="Fixing the offset of vertical CJK text">#3494</a> <a href="/mozilla/pdf.js/commit/95bb727931acd2776bb51544b3864d963f4f1f15" class="message" data-pjax="true" title="Merge pull request #3494 from SSk123/master

Fixing the offset of vertical CJK text">from SSk123/master</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-20T10:26:13-07:00" title="2013-07-20 10:26:13">July 20, 2013</time></span></td>
    </tr>
    <tr class="">
      <td class="icon">
        <span class="octicon octicon-file-directory"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/tree/master/test" class="js-directory-link" id="098f6bcd4621d373cade4e832627b4f6-257a55a954bf09a72915ac0d482d38f4c46ce222" title="test">test</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/0dd0e2ee6485e668b7a0541bda981f0f70acd6a3" class="message" data-pjax="true" title="Merge pull request #3477 from brendandahl/font-loading

Use dummy font for testing when pdf fonts are loaded.">Merge pull request</a> <a href="https://github.com/mozilla/pdf.js/issues/3477" class="issue-link" title="Use dummy font for testing when pdf fonts are loaded.">#3477</a> <a href="/mozilla/pdf.js/commit/0dd0e2ee6485e668b7a0541bda981f0f70acd6a3" class="message" data-pjax="true" title="Merge pull request #3477 from brendandahl/font-loading

Use dummy font for testing when pdf fonts are loaded.">from brendandahl/font-loading</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-18T07:01:55-07:00" title="2013-07-18 07:01:55">July 18, 2013</time></span></td>
    </tr>
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-directory"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/tree/master/web" class="js-directory-link" id="2567a5ec9705eb7ac2c984033e06189d-20da426626bec92b5ac65e57098f5038ac35cadf" title="web">web</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/81308d153da8db995674df195d8aa74f25f23e29" class="message" data-pjax="true" title="Merge pull request #3516 from Rob--W/crx-dont-parse-querystring

[CRX] Get pdf name from URL instead of query string">Merge pull request</a> <a href="https://github.com/mozilla/pdf.js/issues/3516" class="issue-link" title="[CRX] Get pdf name from URL instead of query string">#3516</a> <a href="/mozilla/pdf.js/commit/81308d153da8db995674df195d8aa74f25f23e29" class="message" data-pjax="true" title="Merge pull request #3516 from Rob--W/crx-dont-parse-querystring

[CRX] Get pdf name from URL instead of query string">from Rob--W/crx-dont-parse-querystring</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-29T10:22:14-07:00" title="2013-07-29 10:22:14">July 29, 2013</time></span></td>
    </tr>
    <tr class="">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/.gitignore" class="js-directory-link" id="a084b794bc0759e7a6b77810e01874f2-516970e9163132ff5dfb02a1a7fad9a576b1c2d3" title=".gitignore">.gitignore</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/19dbeaa23ebf441272fdad193b741ed7be1a3cf2" class="message" data-pjax="true" title="Issue #2008 - Add jshint">Issue</a> <a href="https://github.com/mozilla/pdf.js/issues/2008" class="issue-link" title="Evaluate whether jshint is better than gjslint">#2008</a> <a href="/mozilla/pdf.js/commit/19dbeaa23ebf441272fdad193b741ed7be1a3cf2" class="message" data-pjax="true" title="Issue #2008 - Add jshint">- Add jshint</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-01-31T16:12:44-08:00" title="2013-01-31 16:12:44">January 31, 2013</time></span></td>
    </tr>
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/.jshintrc" class="js-directory-link" id="4d5aa81bf4f18104bb6ea53a8b5d1f43-c70a50b30341500e2fd6b1e414b44bc656b95092" title=".jshintrc">.jshintrc</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/3461d02d055802d0ea018fe616aa4fe78da03c5f" class="message" data-pjax="true" title="Enforces trailing spaces">Enforces trailing spaces</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-01T09:25:46-07:00" title="2013-07-01 09:25:46">July 01, 2013</time></span></td>
    </tr>
    <tr class="">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/.travis.yml" class="js-directory-link" id="354f30a63fb0907d4ad57269548329e3-baa0031d5003b75b611433c7a8d83cc0e63fc050" title=".travis.yml">.travis.yml</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/442bd8bd5a9ab4cde3a1139af87e9e9202325c34" class="message" data-pjax="true" title="Issue #2654 - Lint with Travis-CI">Issue</a> <a href="https://github.com/mozilla/pdf.js/issues/2654" class="issue-link" title="Enable linting with Travis-CI">#2654</a> <a href="/mozilla/pdf.js/commit/442bd8bd5a9ab4cde3a1139af87e9e9202325c34" class="message" data-pjax="true" title="Issue #2654 - Lint with Travis-CI">- Lint with Travis-CI</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-02-01T09:01:04-08:00" title="2013-02-01 09:01:04">February 01, 2013</time></span></td>
    </tr>
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/AUTHORS" class="js-directory-link" id="3d350169560e75d0cf9fc8e3574a3639-42e747ef8f6b18ae05c0572bf3f492f17bdf793d" title="AUTHORS">AUTHORS</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/0099ea70391fc4324a762d3444359d1fa3c0153d" class="message" data-pjax="true" title="Added Chrome extension&#39;s author">Added Chrome extension's author</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-04-04T04:40:42-07:00" title="2013-04-04 04:40:42">April 04, 2013</time></span></td>
    </tr>
    <tr class="">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/LICENSE" class="js-directory-link" id="9879d6db96fd29134fc802214163b95a-e454a52586f29b8ce8a6799163eac1f875e9ac01" title="LICENSE">LICENSE</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/332ae4ce41df7675253991ce53ebfe14091b01ca" class="message" data-pjax="true" title="Change to the Apache v2 license.">Change to the Apache v2 license.</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2012-08-31T15:48:21-07:00" title="2012-08-31 15:48:21">August 31, 2012</time></span></td>
    </tr>
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/README.md" class="js-directory-link" id="04c6e90faac2675aa89e2176d2eec7d8-04b44bc295209e48aa3142f54e63bd1ed7eced78" title="README.md">README.md</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/1bbf0ac50ba460c39f7a5a329a7a28134f3bca5b" class="message" data-pjax="true" title="Update README.md">Update README.md</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-29T13:16:14-07:00" title="2013-07-29 13:16:14">July 29, 2013</time></span></td>
    </tr>
    <tr class="">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/make.js" class="js-directory-link" id="da55c948ab565ebe30a2027fe5b721ab-1e6e8b007a660b6fa5f727e95f5db5a78a3c5fd2" title="make.js">make.js</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/2ab481a1dacf84e8a19ef141e5b57e45052dff22" class="message" data-pjax="true" title="Removes foreign for Firefox CSS prefixes">Removes foreign for Firefox CSS prefixes</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-07-09T15:23:20-07:00" title="2013-07-09 15:23:20">July 09, 2013</time></span></td>
    </tr>
    <tr class="alt">
      <td class="icon">
        <span class="octicon octicon-file-text"></span>
        <img alt="Octocat-spinner-32" class="spinner" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
      </td>
      <td class="content">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/blob/master/package.json" class="js-directory-link" id="b9cfc7f2cdf78a7f4b91a753d10865a2-45e3689fc78ee35c324f3e359794fe31edcabb02" title="package.json">package.json</a></span>
      </td>
      <td class="message">
        <span class="css-truncate css-truncate-target"><a href="/mozilla/pdf.js/commit/63adf812c5bc19224034c1a380701c21d4bc72b5" class="message" data-pjax="true" title="Add PDF.js license to package.json">Add PDF.js license to package.json</a></span>
      </td>
      <td class="age"><span class="css-truncate css-truncate-target"><time class="js-relative-date" datetime="2013-03-27T01:16:40-07:00" title="2013-03-27 01:16:40">March 27, 2013</time></span></td>
    </tr>
</tbody>

  </table>
</div>

  <div id="readme" class="clearfix announce instapaper_body md">
    <span class="name"><span class="octicon octicon-book"></span> README.md</span><article class="markdown-body entry-content" itemprop="mainContentOfPage"><h1>
<a name="pdfjs" class="anchor" href="#pdfjs"><span class="octicon octicon-link"></span></a>PDF.js</h1>

<p>PDF.js is Portable Document Format (PDF) viewer that is built with HTML5.</p>

<p>PDF.js is community-driven and supported by Mozilla Labs. Our goal is to
create a general-purpose, web standards-based platform for parsing and
rendering PDFs.</p>

<h2>
<a name="contributing" class="anchor" href="#contributing"><span class="octicon octicon-link"></span></a>Contributing</h2>

<p>PDF.js is an open source project and always looking for more contributors. To
get involved checkout:</p>

<ul>
<li><a href="https://github.com/mozilla/pdf.js/wiki/Contributing">Workflow</a></li>
<li><a href="https://github.com/mozilla/pdf.js/wiki/Style-Guide">Style Guide</a></li>
<li><a href="https://github.com/mozilla/pdf.js/issues?direction=desc&amp;labels=5-good-beginner-bug&amp;page=1&amp;sort=created&amp;state=open">Good Beginner Bugs</a></li>
<li><a href="https://github.com/mozilla/pdf.js/issues/milestones">Priorities</a></li>
<li><a href="https://github.com/mozilla/pdf.js/wiki/Weekly-Public-Meetings">Attend a Public Meeting</a></li>
</ul><p>For further questions or guidance feel free to stop by #pdfjs on
irc.mozilla.org.</p>

<h2>
<a name="getting-started" class="anchor" href="#getting-started"><span class="octicon octicon-link"></span></a>Getting Started</h2>

<h3>
<a name="online-demo" class="anchor" href="#online-demo"><span class="octicon octicon-link"></span></a>Online demo</h3>

<ul>
<li><a href="http://mozilla.github.io/pdf.js/web/viewer.html">http://mozilla.github.io/pdf.js/web/viewer.html</a></li>
</ul><h3>
<a name="browser-extensions" class="anchor" href="#browser-extensions"><span class="octicon octicon-link"></span></a>Browser Extensions</h3>

<h4>
<a name="firefox" class="anchor" href="#firefox"><span class="octicon octicon-link"></span></a>Firefox</h4>

<p>PDF.js is built into version 19+ of Firefox, however two extensions are still
available that are updated at a different rate:</p>

<ul>
<li>
<a href="http://mozilla.github.io/pdf.js/extensions/firefox/pdf.js.xpi">Development Version</a> - This version is updated every time new code is merged into the PDF.js codebase. This should be quite stable but still might break from time to time.</li>
<li>
<a href="https://addons.mozilla.org/firefox/addon/pdfjs">Stable Version</a> - After version 24 of Firefox is released we no longer plan to support the stable extension. The stable version will then be considered whatever is built into Firefox.</li>
</ul><h4>
<a name="chrome" class="anchor" href="#chrome"><span class="octicon octicon-link"></span></a>Chrome</h4>

<p>The Chrome extension is still somewhat experimental but it can be installed two
ways:</p>

<ul>
<li>
<a href="https://chrome.google.com/webstore/detail/pdf-viewer/oemmndcbldboiebfnladdacbdfmadadm">Unofficial Version</a> - <em>This extension is maintained by a PDF.js contributor.</em>
</li>
<li>Build Your Own - Get the code as explained below and issue <code>node make extension</code>. Then open
Chrome, go to <code>Tools &gt; Extension</code> and load the (unpackaged) extension from the
directory <code>build/chrome</code>.</li>
</ul><h2>
<a name="getting-the-code" class="anchor" href="#getting-the-code"><span class="octicon octicon-link"></span></a>Getting the Code</h2>

<p>To get a local copy of the current code, clone it using git:</p>

<pre><code>$ git clone git://github.com/mozilla/pdf.js.git pdfjs
$ cd pdfjs
</code></pre>

<p>Next, you need to start a local web server as some browsers don't allow opening
PDF files for a file:// url:</p>

<pre><code>$ node make server
</code></pre>

<p>You can install Node via <a href="https://github.com/creationix/nvm">nvm</a> or the
<a href="http://nodejs.org">official package</a>. If everything worked out, you can now
serve</p>

<ul>
<li>http://localhost:8888/web/viewer.html</li>
</ul><p>You can also view all the test pdf files on the right side serving</p>

<ul>
<li>http://localhost:8888/test/pdfs/?frame</li>
</ul><h2>
<a name="building-pdfjs" class="anchor" href="#building-pdfjs"><span class="octicon octicon-link"></span></a>Building PDF.js</h2>

<p>In order to bundle all <code>src/</code> files into a final <code>pdf.js</code> and build the generic
viewer, issue:</p>

<pre><code>$ node make generic
</code></pre>

<p>This will generate the file <code>build/generic/build/pdf.js</code> that can be included in
your final project. The pdf.js file is large and should be minified for
production. Also, if you would like to support more browsers than Firefox you'll
also need to include <code>compatibility.js</code> from <code>build/generic/web/</code>.</p>

<h2>
<a name="learning" class="anchor" href="#learning"><span class="octicon octicon-link"></span></a>Learning</h2>

<p>You can play with the PDF.js API directly from your browser through the live
demos below:</p>

<ul>
<li>Hello world: <a href="http://jsbin.com/pdfjs-helloworld-v2/edit#html,live">http://jsbin.com/pdfjs-helloworld-v2/edit#html,live</a>
</li>
<li>Simple reader with prev/next page controls: <a href="http://jsbin.com/pdfjs-prevnext-v2/edit#html,live">http://jsbin.com/pdfjs-prevnext-v2/edit#html,live</a>
</li>
</ul><p>The repo contains a hello world example that you can run locally:</p>

<ul>
<li><a href="https://github.com/mozilla/pdf.js/blob/master/examples/helloworld/">examples/helloworld/</a></li>
</ul><p>For an introduction to the PDF.js code, check out the presentation by our
contributor Julian Viereck:</p>

<ul>
<li><a href="http://www.youtube.com/watch?v=Iv15UY-4Fg8">http://www.youtube.com/watch?v=Iv15UY-4Fg8</a></li>
</ul><p>You can read more about PDF.js here:</p>

<ul>
<li><a href="http://andreasgal.com/2011/06/15/pdf-js/">http://andreasgal.com/2011/06/15/pdf-js/</a></li>
<li><a href="http://blog.mozilla.com/cjones/2011/06/15/overview-of-pdf-js-guts/">http://blog.mozilla.com/cjones/2011/06/15/overview-of-pdf-js-guts/</a></li>
</ul><p>Even more learning resources can be found at:</p>

<ul>
<li><a href="https://github.com/mozilla/pdf.js/wiki/Additional-Learning-Resources">https://github.com/mozilla/pdf.js/wiki/Additional-Learning-Resources</a></li>
</ul><h2>
<a name="questions" class="anchor" href="#questions"><span class="octicon octicon-link"></span></a>Questions</h2>

<p>Talk to us on IRC:</p>

<ul>
<li>#pdfjs on irc.mozilla.org</li>
</ul><p>Join our mailing list:</p>

<ul>
<li><a href="mailto:dev-pdf-js@lists.mozilla.org">dev-pdf-js@lists.mozilla.org</a></li>
</ul><p>Subscribe either using lists.mozilla.org or Google Groups:</p>

<ul>
<li><a href="https://lists.mozilla.org/listinfo/dev-pdf-js">https://lists.mozilla.org/listinfo/dev-pdf-js</a></li>
<li><a href="https://groups.google.com/group/mozilla.dev.pdf-js/topics">https://groups.google.com/group/mozilla.dev.pdf-js/topics</a></li>
</ul><p>Follow us on twitter: @pdfjs</p>

<ul>
<li><a href="http://twitter.com/#!/pdfjs">http://twitter.com/#!/pdfjs</a></li>
</ul><p>Weekly Public Meetings</p>

<ul>
<li><a href="https://github.com/mozilla/pdf.js/wiki/Weekly-Public-Meetings">https://github.com/mozilla/pdf.js/wiki/Weekly-Public-Meetings</a></li>
</ul></article>
  </div>


        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/">
      <span class="mega-octicon octicon-mark-github"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2013 <span title="0.09017s from fe17.rs.github.com">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
          <div class="suggester-container">
              <div class="suggester fullscreen-suggester js-navigation-container" id="fullscreen_suggester"
                 data-url="/mozilla/pdf.js/suggestions/commit">
              </div>
          </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped leftwards" title="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped leftwards"
      title="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-remove-close close ajax-error-dismiss"></a>
      Something went wrong with that request. Please try again.
    </div>

    
  </body>
</html>

